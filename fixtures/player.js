module.exports = {
    bonner: {
        email: 'FrederickKBonner@dayrep.com',
        name: 'Frederick K. Bonner',
        id: 'PL-BdiTUIO'
    },
    adams: {
        email: 'JohnRAdams@armyspy.com',
        name: 'John R. Adams',
        id: 'PL-AdiTjIO'
    },
};
