module.exports = {
    id: {
        type: 'string',
        minLength: 24,
        maxLength: 24 // RS-DeXGA947IFAAFKRSBJ-CL
        // TODO probably add regex pattern
    }
};
